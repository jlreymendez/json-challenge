﻿using System;
using System.IO;
using Code.Data;
using Code.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Code.Parser
{
    public class JsonLiveParser
    {
        public JsonLiveParser(string filename, ILogger logger)
        {
            _filename = filename;
            _logger = logger;
        }

        public bool HasUpdate()
        {
            return _lastWriteTime < File.GetLastWriteTime(_filename);
        }

        public TableContent Parse()
        {
            var result = TableContent.Empty;

            if (File.Exists(_filename) == false)
            {
                return result;
            }

            using (StreamReader reader = File.OpenText(_filename))
            {
                JObject json = (JObject) JToken.ReadFrom(new JsonTextReader(reader));

                var titleToken = json.GetValue(TitleKey);
                result.Title = GetStringValue(titleToken, TitleKey);

                var columnToken = json.GetValue(ColumnHeadersKey);
                if (columnToken?.Type == JTokenType.Array)
                {
                    var columnArray = (JArray) columnToken;
                    result.Columns = new string[columnArray.Count];
                    for (var i = 0; i < columnArray.Count; i++)
                    {
                        var columnKey = $"{ColumnHeadersKey}[{i}]";
                        result.Columns[i] = GetStringValue(columnArray[i], columnKey);
                    }
                }
                else
                {
                    _logger.LogError("{0}: Expected an Array, received {1}", ColumnHeadersKey, columnToken?.Type);
                }

                var dataToken = json.GetValue(DataKey);
                if (dataToken?.Type == JTokenType.Array)
                {
                    var dataArray = (JArray) dataToken;
                    result.Data = new string[dataArray.Count][];
                    for (var dataIndex = 0; dataIndex < dataArray.Count; dataIndex++)
                    {
                        var dataKey = $"{DataKey}[{dataIndex}]";
                        result.Data[dataIndex] = new string[result.Columns.Length];
                        if (dataArray[dataIndex].Type == JTokenType.Object)
                        {
                            var dataObject = (JObject) dataArray[dataIndex];
                            for (var columnIndex = 0; columnIndex < result.Columns.Length; columnIndex++)
                            {
                                var dataColumnKey = $"{dataKey}[{result.Columns[columnIndex]}]";
                                var columnValue = dataObject.GetValue(result.Columns[columnIndex]);
                                result.Data[dataIndex][columnIndex] = GetStringValue(columnValue, dataColumnKey);
                            }
                        }
                        else
                        {
                            _logger.LogError(
                                "{0}: Expected an Object, received {1}", dataKey, dataArray[dataIndex].Type);
                        }
                    }
                }
                else
                {
                    _logger.LogError("{0}: Expected an Array, received {1}", DataKey, dataToken?.Type);
                }

                _lastWriteTime = File.GetLastWriteTime(_filename);
            }

            return result;
        }

        string GetStringValue(JToken token, string key)
        {
            var result = "";
            switch (token?.Type)
            {
                case JTokenType.String:
                    result = token.Value<string>();
                    break;
                case JTokenType.Integer:
                    result = token.Value<int>().ToString();
                    break;
                case JTokenType.Float:
                    result = token.Value<float>().ToString("F");
                    break;
                case JTokenType.Boolean:
                    result = token.Value<bool>().ToString();
                    break;
                case JTokenType.Date:
                    result = token.Value<DateTime>().ToString("s");
                    break;
                default:
                    _logger.LogError(
                        "{0}: Unexpected type on get string value, received {1}.", key, token?.Type);
                    break;

            }

            return result;
        }

        const string TitleKey = "Title";
        const string ColumnHeadersKey = "ColumnHeaders";
        const string DataKey = "Data";

        ILogger _logger;
        string _filename;
        DateTime _lastWriteTime = DateTime.MinValue;
    }
}