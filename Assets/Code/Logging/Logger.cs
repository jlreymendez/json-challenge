﻿using Debug = UnityEngine.Debug;

namespace Code.Logging
{
    public class Logger : ILogger
    {
        public void LogInfo(string message, params object[] messageValues)
        {
            Debug.LogFormat(message, messageValues);
        }

        public void LogWarning(string message, params object[] messageValues)
        {
            Debug.LogWarningFormat(message, messageValues);
        }

        public void LogError(string message, params object[] messageValues)
        {
            Debug.LogErrorFormat(message, messageValues);
        }
    }
}