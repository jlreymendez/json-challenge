﻿using UnityEngine;

namespace Code.Data
{
    [System.Serializable]
    public struct CellStyle
    {
        public int FontSize;
        public FontStyle FontStyle;
    }
}