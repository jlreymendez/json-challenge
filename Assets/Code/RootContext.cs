﻿using Code.Parser;
using Code.Utils;
using UnityEngine;

namespace Code
{
    public class RootContext : MonoBehaviour
    {
        void Awake()
        {
            _logger = new Logging.Logger();

            _gameObjectPool = new GameObjectPool(_logger);

            _parser = new JsonLiveParser(JsonFilePath, _logger);

            _tableBuilder = Instantiate(_tableBuilderPrefab);
            _tableBuilder.Parser = _parser;
            _tableBuilder.GameObjectPool = _gameObjectPool;
        }

        [SerializeField] UiTableBuilder _tableBuilderPrefab;

        Logging.ILogger _logger;
        JsonLiveParser _parser;
        GameObjectPool _gameObjectPool;
        UiTableBuilder _tableBuilder;

        static readonly string JsonFilePath = Application.streamingAssetsPath + "/JsonChallenge.json";
    }
}