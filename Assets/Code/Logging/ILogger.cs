﻿namespace Code.Logging
{
    public interface ILogger
    {
        void LogInfo(string message, params object[] messageValues);
        void LogWarning(string message, params object[] messageValues);
        void LogError(string message, params object[] messageValues);
    }
}