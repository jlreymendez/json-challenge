﻿namespace Code.Data
{
    public struct TableContent
    {
        public string Title;
        public string[] Columns;
        public string[][] Data;

        public static TableContent Empty => new TableContent {Columns = new string[0], Data = new string[0][]};
    }
}