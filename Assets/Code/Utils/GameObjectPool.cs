﻿using System.Collections.Generic;
using UnityEngine;
using ILogger = Code.Logging.ILogger;

namespace Code.Utils
{
    public class GameObjectPool
    {
        Dictionary<string, Stack<GameObject>> _pools = new Dictionary<string, Stack<GameObject>>();
        ILogger _logger;
        public GameObjectPool(ILogger logger)
        {
            _logger = logger;
        }

        public void Create(string poolName, GameObject prefab)
        {
        #if DEBUG
            if (_pools.ContainsKey(poolName))
            {
                _logger.LogWarning($"Attempting to create existing pool with name:{poolName}");
            }
        #endif

            var pool = new Stack<GameObject>();
            pool.Push(prefab);
            _pools[poolName] = pool;
        }

        public void PreAllocate(string poolName, uint amount)
        {
        #if DEBUG
            if (_pools.ContainsKey(poolName) == false)
            {
                _logger.LogError($"Attempting to preallocate non-existing pool with name:{poolName}");
            }
        #endif

            var stack = _pools[poolName];
        #if DEBUG
            if (stack.Count > 1)
            {
                _logger.LogError($"Attempting to preallocate non-empty pool with name:{poolName}");
            }
        #endif

            var prefab = stack.Peek();
            for (var i = 0; i < amount; i++)
            {
                var clone = GameObject.Instantiate(prefab);
                clone.SetActive(false);
                stack.Push(clone);
            }
        }

        public GameObject Take(string poolName)
        {
        #if DEBUG
            if (_pools.ContainsKey(poolName) == false)
            {
                _logger.LogError($"Attempting to take object from non-existing pool with name:{poolName}");
            }
        #endif

            var stack = _pools[poolName];
            if (stack.Count > 1)
            {
                return stack.Pop();
            }

            var prefab = stack.Peek();
            return GameObject.Instantiate(prefab);
        }

        public T Take<T>(string poolName) where T : MonoBehaviour
        {
            var go = Take(poolName);
            return go?.GetComponent<T>();
        }

        public void Give(string poolName, GameObject gameObject)
        {
        #if DEBUG
            if (_pools.ContainsKey(poolName) == false)
            {
                _logger.LogError($"Attempting to give object to non-existing pool with name:{poolName}");
            }

            if (gameObject == null)
            {
                _logger.LogError($"Attempting to give null object to pool with name:{poolName}");
            }
        #endif

            gameObject.SetActive(false);
            _pools[poolName].Push(gameObject);
        }
    }
}