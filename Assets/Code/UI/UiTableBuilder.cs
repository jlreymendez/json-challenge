﻿using System;
using System.Collections.Generic;
using System.IO;
using Code.Data;
using Code.Parser;
using Code.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Code
{
    public class UiTableBuilder : MonoBehaviour
    {
        public GameObjectPool GameObjectPool { private get; set; }
        public JsonLiveParser Parser { private get; set; }

        void Start()
        {
            if (GameObjectPool == null || Parser == null)
            {
                enabled = false;
                return;
            }

            GameObjectPool.Create(RowPoolName, _rowPrefab.gameObject);
            GameObjectPool.Create(CellPoolName, _cellPrefab.gameObject);

            _rowElements = new List<GameObject>();
            _cellElements = new List<GameObject>();
        }

        void Update()
        {
            if (Parser.HasUpdate())
            {
                var tableContent = Parser.Parse();
                RecycleObjects(_rowElements, RowPoolName);
                RecycleObjects(_cellElements, CellPoolName);
                BuildTable(tableContent);
            }
        }

        void RecycleObjects(List<GameObject> elements, string poolName)
        {
            foreach (var element in elements)
            {
                GameObjectPool.Give(poolName, element);
            }
            elements.Clear();
        }

        void BuildTable(in TableContent content)
        {
            var rowIndex = 0;
            var titleRow = AddRow(rowIndex++);
            AddCellToRow(0, titleRow, content.Title, _titleStyle);
            titleRow.SetActive(true);

            var headerRow = AddRow(rowIndex++);
            var columnIndex = 0;
            foreach (var column in content.Columns)
            {
                AddCellToRow(columnIndex++, headerRow, column, _headerStyle);
            }
            headerRow.SetActive(true);

            foreach (var data in content.Data)
            {
                var dataRow = AddRow(rowIndex++);
                columnIndex = 0;
                foreach (var dataValue in data)
                {
                    AddCellToRow(columnIndex++, dataRow, dataValue, _dataStyle);
                }
                dataRow.SetActive(true);
            }
        }

        GameObject AddRow(int rowIndex)
        {
            var row = GameObjectPool.Take(RowPoolName);
            row.transform.SetParent(transform);
            row.transform.SetSiblingIndex(rowIndex);
            _rowElements.Add(row);
            return row;
        }

        void AddCellToRow(int columnIndex, GameObject row, string content, CellStyle style)
        {
            var cell = GameObjectPool.Take<Text>(CellPoolName);
            cell.text = content;
            cell.fontStyle = style.FontStyle;
            cell.fontSize = style.FontSize;
            cell.transform.SetParent(row.transform);
            cell.transform.SetSiblingIndex(columnIndex);
            cell.gameObject.SetActive(true);
            _cellElements.Add(cell.gameObject);
        }

        [SerializeField] HorizontalLayoutGroup _rowPrefab = default;
        [SerializeField] Text _cellPrefab = default;
        [SerializeField] CellStyle _titleStyle = default;
        [SerializeField] CellStyle _headerStyle = default;
        [SerializeField] CellStyle _dataStyle = default;

        List<GameObject> _rowElements;
        List<GameObject> _cellElements;

        const string RowPoolName = "UiTableRowPool";
        const string CellPoolName = "UiTableCellPool";
    }
}
